package ar.com.spring.cloud.poc.client.client.messageprinter;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;


@Component
public class MessagePrinter {
    @Value("${string.message.number1}")
    private String message1;
    @Value("${string.message.number2}")
    private String message2;
    @Value("${string.message.number3}")
    private String message3;

    @Scheduled(initialDelay = 1500,fixedDelay = 500)
    public void publish() {
        System.out.println(message1);
        System.out.println(message2);
        System.out.println(message3);
    }


}
