package ar.com.spring.cloud.poc.client.client;

import ar.com.spring.cloud.poc.client.client.messageprinter.MessagePrinter;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class ClientApplication {

	public static void main(String[] args) {
		SpringApplication.run(ClientApplication.class, args);
		MessagePrinter messagePrinter = new MessagePrinter();
	}
}
